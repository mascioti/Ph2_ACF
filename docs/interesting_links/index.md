# Some links

Here you can check some interesting links in English.

This is also a good practice to gather all the single links that you want to publish, instead of putting them directly on the `mkdocs.yml` as sections of your documentation.

- **[CERN's Homepage in MD format](https://home.cern)**
- **CERN's Homepage raw link**: <https://home.cern>
- **[s2i MkDocs Example in a new tab](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example){target=_blank}**: Notice that this needs [the extension attr_list](https://github.com/mkdocs/mkdocs/issues/1958#issuecomment-633846122){target=_blank} in the `markdown_extensions` of your `mkdocs.yml`.
- **HTML link in a new tab**: <a href="https://www.mkdocs.org" target="_blank">MkDocs</a>
