#include "tools/ConfigureOnly.h"
#include "System/RegisterHelper.h"

std::string ConfigureOnly::fCalibrationDescription = "Run only configuration step";

ConfigureOnly::ConfigureOnly() : Tool() {}

ConfigureOnly::~ConfigureOnly() {}

void ConfigureOnly::Running() {}

void ConfigureOnly::Stop() {}

void ConfigureOnly::Pause() {}

void ConfigureOnly::Resume() {}
